module.exports = {
  env: {
    appName: 'bucket.list',
    year: '2021',
    appstoreLink: 'https://www.apple.com/app-store/',
    playstoreLink: 'https://play.google.com/store',
  },
};
