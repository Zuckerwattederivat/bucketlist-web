# <p align="center">bucket.list Web - A promotion page for the bucket.list iOS and Android application</p>

bucket.list is an application for users to create their own bucketlists, comprised of thins they always wanted to do and share those lists with an ever-growing community of people. This repository hosts the code for the bucket.list promotion page, which is built with Next.js.

## Contents :file_folder:

- [Setup](https://github.com/Zuckerwattederivat/bucketlist-web#setup-wrench)
- [Support](https://github.com/Zuckerwattederivat/bucketlist-web#support-ambulance)
- [License](https://github.com/Zuckerwattederivat/bucketlist-web#license-scroll)

## Setup :wrench:

```
# clone the repositiory with git using https
$ git clone https://github.com/Zuckerwattederivat/bucketlist-web.git

# clone the repositiory with git using ssh
$ git clone git@github.com:Zuckerwattederivat/bucketlist-web.git

# clone the repositiory with git using gh cli
$ gh repo clone Zuckerwattederivat/bucketlist-web

# install dependencies
$ cd bucketlist-web
$ npm install

# run project in dev mode
$ cd bucketlist-web
$ npm run dev

# build project
$ cd bucketlist-web
$ npm run build

# run project in production mode
$ cd bucketlist-web
$ npm start

```

## Branching conventions

### Features or Modules

Features or modules are counted incrementally starting at 1. Please refer to the existing branches or commit section to see which number to use.

`feature/M-${featureNumber}-${featureName}`

### Bug Fixes

For bug fixes please refer to the corresponding feature number

`bugfix/B-${featureNumber}-${featureName}`

## Support :ambulance:

Use the issue ticker to submit bugs or requests. :blush:

## License :scroll:

This project is not licensed for open-source usage [License](https://github.com/Zuckerwattederivat/bucketlist-web/blob/master/LICENSE.md).
