import Head from 'next/head';
import Hero from '../components/modules/Hero';

export default function Home() {
  return (
    <>
      <Head>
        <title>bucket.list</title>
        <meta
          name='description'
          content='Promotion page for the bucket.list iOS and Android application'
        />
        <link
          rel='apple-touch-icon'
          sizes='180x180'
          href='/apple-touch-icon.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='32x32'
          href='/favicon-32x32.png'
        />
        <link
          rel='icon'
          type='image/png'
          sizes='16x16'
          href='/favicon-16x16.png'
        />
        <link rel='manifest' href='/site.webmanifest' />
        <link rel='mask-icon' href='/safari-pinned-tab.svg' color='#726af8' />
        <meta name='msapplication-TileColor' content='#ffeeb4' />
        <meta name='theme-color' content='#ffeeb4' />
        <link rel='preconnect' href='https://fonts.gstatic.com' />
        <link
          href='https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,600;0,700;1,400;1,600;1,700&display=swap'
          rel='stylesheet'
        />
      </Head>

      <Hero />
    </>
  );
}
