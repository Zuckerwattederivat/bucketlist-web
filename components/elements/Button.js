import Link from 'next/link';

import classes from '../../styles/modules/elements/button.module.scss';

const Button = ({ children, link, onClick, icon, style }) => {
  return link ? (
    <Link href={link}>
      <a
        className={`${classes.btn} ${
          style === 'secondary' ? classes.secondary : ''
        } ${style === 'outlined' ? classes.outlined : ''}`}
      >
        <span>{children}</span>
        {icon && <span>{icon}</span>}
      </a>
    </Link>
  ) : (
    <button
      className={`${classes.btn} ${
        style === 'secondary' ? classes.secondary : ''
      } ${style === 'outlined' ? classes.outlined : ''}`}
      onClick={onClick}
    >
      <span>{children}</span>
      {icon && <span>{icon}</span>}
    </button>
  );
};

export default Button;
