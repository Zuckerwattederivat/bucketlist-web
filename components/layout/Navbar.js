import Link from 'next/link';
import Button from '../elements/Button';
import { FaCloudDownloadAlt } from 'react-icons/fa';

import classes from '../../styles/modules/layout/navbar.module.scss';

const Navbar = () => {
  return (
    <nav className={classes.navbar}>
      <Link href='/'>
        <a className={classes.logoLink}>
          <img src='/logo-1.svg' alt='logo' />
          <span>{process.env.appName.split('.')[0]}.</span>
          <span>{process.env.appName.split('.')[1]}</span>
        </a>
      </Link>
      <div className={classes.btnContainer}>
        <Button
          link='https://www.apple.com/app-store/'
          icon={<FaCloudDownloadAlt />}
          style='outlined'
        >
          Download
        </Button>
      </div>
    </nav>
  );
};

export default Navbar;
