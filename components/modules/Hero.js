import Button from '../elements/Button';
import { FaCloudDownloadAlt, FaCompass } from 'react-icons/fa';

import classes from '../../styles/modules/hero.module.scss';

const HeroModule = () => {
  return (
    <section className={classes.hero}>
      <div className={classes.mainContent}>
        <div className={classes.text}>
          <h1>
            <span>
              Weil <span className='text-primary'>wir</span> eher bereuen etwas
              nicht getan zu haben!
            </span>
          </h1>
          <p>
            <span>
              <span className='text-primary'>bucket.list</span> ist die
              ToDo-Liste für’s Leben.
            </span>
          </p>
          <div className={classes.btnContainer}>
            <Button
              link='https://www.apple.com/app-store/'
              icon={<FaCloudDownloadAlt />}
              style='primary'
            >
              Get the app
            </Button>
            <Button link='/' icon={<FaCompass />} style='secondary'>
              Discover
            </Button>
          </div>
        </div>
        <div className={classes.heroImage}>
          <picture>
            <img className='img-fluid' src='/hero-image-3.png' alt='hero' />
          </picture>
        </div>
        <div className={classes.dottedLines}>
          <svg
            width='1200px'
            height='613'
            viewBox='0 0 803 613'
            fill='none'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              d='M1139.86 3.34047C1084.81 66.5727 945.466 213.924 828.556 297.471C682.418 401.905 597.647 486.015 515.85 459.615C434.053 433.215 380.041 346.672 447.263 301.485C514.485 256.299 578.733 321.623 603.553 354.157C628.372 386.691 658.98 537.022 539.162 591.986C419.343 646.949 305.43 570.055 230.971 472.453C156.512 374.851 77.6384 344.936 3.13721 336.497'
              stroke='#726AF8'
              strokeWidth='5'
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeDasharray='2 10'
            />
          </svg>
        </div>
      </div>
      <div className={classes.dots}>
        <picture>
          <img src='/doodle-dots.png' alt='doodle-dots' className='img-fluid' />
        </picture>
      </div>
      <div className={classes.appLinkContainer}>
        <a
          href={process.env.appstoreLink}
          rel='noopener noreferrer'
          target='_blank'
        >
          <picture>
            <img src='/appstore.png' alt='appstore' />
          </picture>
        </a>
        <a
          href={process.env.playstoreLink}
          rel='noopener noreferrer'
          target='_blank'
        >
          <picture>
            <img src='/playstore.png' alt='playstore' />
          </picture>
        </a>
      </div>
    </section>
  );
};

export default HeroModule;
